FROM python:3.6.6-alpine3.8

ENV PYTHONUNBUFFERED 1

RUN apk add --no-cache --virtual .build-deps \
  ca-certificates gcc g++ \
  postgresql-dev linux-headers musl-dev \
  gettext gettext-dev \
  libffi-dev jpeg-dev zlib-dev \
  mailcap pcre-dev gzip \
  busybox-extras

RUN pip install --upgrade pip

RUN pip install pandas==1.1.4 numpy==1.19.3 --no-cache-dir

RUN pip install uwsgi -I --no-cache-dir

EXPOSE 8000
