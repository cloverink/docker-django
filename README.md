# This docker has
- alpine 3.8
- ca-certificates 
- gcc g++
- postgresql-dev
- gettext
- gzip
- python 3.6.6
- pip
- uwsgi 
- pandas 0.23.4

### and EXPOSE 8000

and you can pull from 
```
registry.gitlab.com/cloverink/docker-django:latest
```

good luck ...